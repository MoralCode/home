"""Automate the process of adding people to a matrix room based on pretix.eu registrations

For authenticating to pretix, please supply the following variables in a .env file:
```
ACCESS_TOKEN=[TOKEN]
EVENT_NAME=f40-party
EVENT_ORGANIZER=fedora
```


Dependencies:
- dotenv


Examples:

mark processed (with .env moved): python3 ./pretix-matrix.py --csvfile ../../6-parsed.csv --mark-processed

run new batch (with .env in place): python3 ./pretix-matrix.py --filter-processed 

"""
import requests
import csv
from typing import List, Dict
from functools import reduce
import argparse
import os
import sys
from dotenv import load_dotenv

def fetch_data(bearer_token: str, url: str) -> dict:
    headers = {
        "Authorization": f"Bearer {bearer_token}"
    }
    data = []

    while url:
        response = requests.get(url, headers=headers)
        response.raise_for_status()
        json_response = response.json()
        data.extend(json_response.get('results', []))
        url = json_response.get('next')
    return data

def question_id_to_header(question_id:str):
    if question_id == "fas":
        return "Fedora Account Services (FAS)"
    elif question_id == "matrix":
        return "Matrix ID"
    
    return ""

def extract_answers(schema: dict) -> List[dict]:
    def reducer(entries: Dict[str, dict], result: dict) -> Dict[str, dict]:
        for position in result.get('positions', []):
            ticket_id = position['order']
            if not entries.get(ticket_id):
                entries[ticket_id] = {
                    'Order code': ticket_id,
                    'Email': result.get('email', ''),
                    "Order datetime": result.get("datetime", ''),
                    "Pseudonymization ID": position.get("pseudonymization_id", ''),
                    "Fedora Account Services (FAS)": '',
                    "Matrix ID": '',
                    "Invoice address name": result.get('invoice_address', {}).get('name', ''),
                }
            for answer in position.get('answers', []):
                if answer['question_identifier'] in {'matrix', 'fas'}:
                    entries[ticket_id][question_id_to_header(answer['question_identifier'])] = answer['answer']  # noqa: E501
        return entries

    reduced_results = reduce(reducer, schema, {})
    return list(reduced_results.values())


def write_to_csv(entries: List[dict], file_name: str, display: bool = False) -> None:  # noqa: E501
    fieldnames = entries[0].keys()
    with open(file_name, mode='w+', newline='') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for entry in entries:
            writer.writerow(entry)

        if not display:
            return

        csv_file.flush()
        csv_file.seek(0)
        print(csv_file.read())

def filter_dict(old_dict, your_keys):
    """filters a dictionary so it only contains the specified keys
    accomplishes this by constructing a new dictionary
    """
    return { your_key: old_dict[your_key] for your_key in your_keys }


def csv_to_data(csv_file:str) -> list[dict]:
    """_summary_

    Args:
        csv_file (str): the input filename to process

    Returns:
        list[dict]: the csv data in dict format for further processing
    """
    
    with open(csv_file) as csvfile:
        reader = csv.DictReader(csvfile)
        return list(reader)

def cleanup_csv_for_humans(csv_data:list[dict], filter_keys=["Order code", "Email", "Order date", "Order time", "Pseudonymization ID", "Fedora Account Services (FAS)", "Matrix ID", "Invoice address name"]) -> list[dict]:
    """Takes in a CSV data (dict-formatted) and returns dict-formatted data with unused columns removed

    Args:
        csv_data (list[dict]): the input csv data to process

    Returns:
        list[dict]: the data with unused columns removed
    """
    return [filter_dict(d, filter_keys) for d in csv_data]
   

def filter_processed_data(csv_data:list[dict], processed_csv_data:list[dict], filter_key:str="Order code") -> list[dict]:
    """filters csv data to remove data thats already been processed


    Args:
        csv_data (list[dict]): the input csv data to process
        processed_csv_data (list[dict]): the input csv data containing processed records to filter out
    
    Returns:
        list[dict]: the filtered version of the initial data with already-processed rows removed
    """
    
    processed_ids = set([ r[filter_key] for r in processed_csv_data])

    return list(filter(lambda d: d[filter_key] not in processed_ids, csv_data))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process event orders.')
    
    parser.add_argument('--csvfile', type=str, help='csv filename downloaded from pretix')
    parser.add_argument('--filter-processed', action="store_true", help='optional argument. set this to filter out already-processed entries based on the contents of the file at processed.csv')
    parser.add_argument('--mark-processed', action="store_true", help="update the list of processed entries with all items from the provided csv file")

    load_dotenv()

    args = parser.parse_args()

    PROCESSED_FILE_NAME = "./processed.csv"

    organizer = os.getenv('EVENT_ORGANIZER')
    event = os.getenv('EVENT_NAME')
    token = os.getenv('ACCESS_TOKEN')
    # TODO: accept client ID and secret to also handle the OAUTH request process

    has_credentials = organizer is not None and event is not None and token is not None

    if has_credentials:
        event_url = f"https://pretix.eu/api/v1/organizers/{organizer}/events/{event}/orders/"  # noqa: E501
        data = fetch_data(token, event_url)
        entries = extract_answers(data)
        print(len(data))

    else:
        print("No credentials found via .env...")

        if not args.csvfile:
            print("no data found via --csvfile argument - unable to continue")
            sys.exit(1)
        
        print("Continuing with provided csv data")
    
    if args.mark_processed:
        rows = csv_to_data(args.csvfile)
        rows = cleanup_csv_for_humans(rows, filter_keys=["Order code"])
        write_to_csv(rows, PROCESSED_FILE_NAME)
        sys.exit(0)


    write_to_csv(entries, "matrix_answers.csv", display=True)


    # TODO: future second path: manually-downloaded CSV
    # rows = csv_to_data(args.csvfile)
    # rows = cleanup_csv_for_humans(rows)

    # write_to_csv(rows, args.outfile, display=False)

    prevrows = csv_to_data(PROCESSED_FILE_NAME) if args.filter_processed else None

    if prevrows is not None:
        rows = filter_processed_data(entries, prevrows)

    if len(rows) == 0:
        print("no new rows to process")
    else:
        write_to_csv(rows, "./new.csv", display=True)



