include::ROOT:partial$attributes.adoc[]

= Community Blog

The {COMMBLOG}[Fedora Community Blog] (CommBlog) is a WordPress-based site that delivers news, updates, and calls for help from contributors across Fedora.

image::https://fedoraproject.org/w/uploads/0/09/Community_Blog_screenshot.png[Community Blog screenshot,90%,90%]


[[content]]
== Content

The Community Blog focuses on updates and information about the Fedora Project community.
Topics vary, but mostly include…

* News from specific sub-projects
* Updates on new features or changes in Fedora
* Calls for help from other contributors
* Official {FWIKI}/Elections[Fedora Elections] coverage
* Anything related to Fedora _contributor_ community


[[magazine-vs-community-blog]]
== Fedora Magazine vs. Community Blog

What is the difference between the https://fedoramagazine.org[Fedora Magazine] and the Community Blog?
They focus on two different audiences (or types of readers).

The Fedora Magazine targets everyday Fedora users, Linux enthusiasts, and technology hobbyists.
Fedora contributors are included in these audiences, but they are not the main focus.
The Community Blog is the news hub for Fedora contributors and people working inside of the Fedora community.
The Community Blog specifically focuses on our contributors.

For example, an article on how to use a new package in Fedora is better for the Magazine.
Any Fedora user may be interested in using the package.
An article that announces a new feature to a Fedora Infrastructure service or an Ambassador event report is better for the Community Blog.
A Fedora user may not be interested, but our contributors have a stronger interest.

Sometimes there is some overlap between the two.
Think hard about who you are writing for and who you want to read your article.
This should answer the question of where to write your article.
The CommOps and Fedora Magazine teams are happy to answer any doubts or questions if you are unsure.


[[write-article]]
== Write an article

The Community Blog is managed by the CommOps team.
The system administration is managed by {FWIKI}/Infrastructure[Fedora Infrastructure].
However, we need _your help_ to share the awesome things you and your area of Fedora are working on.

Articles don't need to be long.
Short snippets are useful too.
If you're not sure how much to write, 300 words is a good target.
If needed, you can always write more.

See the {COMMBLOG}/writing-community-blog-article/[Writing an article] guide to get started.
This page explains the process for writing and how to get your post published.


[[community-blog-theme]]
== Community Blog theme

The Community Blog WordPress theme is open source.
Find the source code https://pagure.io/communityblog-theme[on Pagure].
Open a ticket if you find a bug, problem, or something to improve.
Pull requests are also welcome!
Before submitting a pull request, post to the {DISCOURSE}[CommOps Discourse forum] first and ask for feedback on your planned contribution.


[[editors]]
== Editors

Community Blog editors review article drafts and schedule new posts.
Editors are reachable on the {DISCOURSE}[CommOps Discourse forum].
The Discourse forum is the best way to reach an editor.

Below, you can find a current list of editors:

.Community Blog editors
|===
|Name |FAS account |IRC nickname

|Ben Cotton (_editor-in-chief_)
|{FWIKI}/User:Bcotton[bcotton]
|bcotton

|Brian Exelbierd
|{FWIKI}/User:Bex[bex]
|bexelbie

|Brian Proffitt
|{FWIKI}/User:Bproffit[bproffit]
|bkp

|Jona Azizaj
|{FWIKI}/User:Jonatoni[jonatoni]
|jonatoni

|Matthew Miller
|{FWIKI}/User:Mattdm[mattdm]
|mattdm

|Nick Bebout
|{FWIKI}/User:Nb[nb]
|nb

|Sumantro Mukherjee
|{FWIKI}/User:Sumantrom[sumantrom]
|sumantrom

|===
