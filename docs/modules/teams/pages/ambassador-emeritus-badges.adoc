include::ROOT:partial$attributes.adoc[]

https://pagure.io/fedora-badges/issue/632[Ambassador Emeritus Badge]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The Ambassador Emeritus Badge is awarded for retiring from the Fedora Ambassador Team.

image:https://pagure.io/fedora-badges/issue/raw/files/d689147b14b843b82784f04d28213a6ae98c825055b64003a5bb8f57b9d07da7-emeritus.png[alt="emeritus"]
